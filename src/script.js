///this is just an example page! ignore me :D



// 1
const { PrismaClient } = require("@prisma/client")

// 2
const prisma = new PrismaClient()

const newUser = await prisma.user.create({
    data: {
      fname: 'Blob',
      lname: 'Potato',
    },
  })

//3
async function main() {
    const newUser = await prisma.user.create(newUser)
    const allUsers = await prisma.user.findMany()
    console.log(allUsers)
}

//4
main()
  .catch(e => {
    throw e
  })
  // 5
  .finally(async () => {
    await prisma.$disconnect()
  })