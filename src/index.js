const { ApolloServer, gql } = require('apollo-server')
const { PrismaClient } = require("@prisma/client")

const typeDefs = gql`
    type User {
        id: ID!
        fname: String
        lname: String
    }

    type Mutation {
        post(id: ID!, fname: String!, lname: String!): User
    }

    type Query {
        users: [User]
    }

`

const resolvers = {
    Query: {
      users: async (parent, args, context) => {
        return context.prisma.user.findMany()
      },
    },
    Mutation: {
        // 2
        post: (parent, args, context, info) => {
           const newUser = context.prisma.user.create({
                data: {
                    fname: args.fname,
                    lname: args.lname,
                },  
            }) 
          return newUser
        },
      },
  }

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const prisma = new PrismaClient()

const server = new ApolloServer({ 
    typeDefs, 
    resolvers, 
    context: {
        prisma,
    } 
})

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});